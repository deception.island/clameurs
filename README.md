# clameurs

Prototype de "clameurs" (c.f. "La zone du dehors" de A. Damasio).

==

Construit sur une base d'ESP32 (pour sa puisance et son DAC interne, permettant de jouer des MP3, ses 4Mo de flash interne, permettant de stocker assez de sons, son "ultra low power" processor, et son petit prix).

Les autres composants sont un detecteur PIR, un ampli son, une batterie lithium, un "LDO" (low dropout voltage regulator) pour passer à 3.3v.

Notes:
- Il doit consommer très peu. L'ESP32 peut se mettre en "deep sleep" (en ayant toujours l'ULP en fonctionnement). Mais il faut aussi que les autres composants ne pompent pas trop. En particulier l'ampli audio quand il ne joue pas de son. Quasi tous les IC amplis audios ont un mode "SHUTDOWN" qui permet de faire passer la conso à quelques uA. Controlez la conso des choses: j'obtiens environ 20uA pour le capteur PIR, moins d'1 uA pour l'ampli audio en SHUTDOWN, et quelques centaines de uA pour l'ESP32. Evitez les régulateurs de tension "fake", qui peuvent consomer beaucoup plus que les régulateurs de "originaux".

- Pour éviter de casser les pieds des gens la nuit, il devrait être capable de savoir quand il fait nuit pour baisser le son, ou ne pas jouer de son du tout. Pour cela, l'idée est d'utiliser une photoresistance et se baser sur le soleil. Cela peut être fait par l'ULP de l'ESP32.

- Prévoir une boite solide, à scéler au ciment, ou avec des "cerflex" plastiques.

- Options: un petit panneau solaire 5V avec un circuit de recharge de batterie.


==

Compilation :

Pour compiler le projet, il faudra installer:
- l'interface arduino.
- dans l'interface arduino, installer la plateforme "ESP32" (par ex. https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/)
- dans l'interface arduino, installez le module "ESP8266Audio".
- installez la toolchain ESP32 ULP (suivre l'install de https://github.com/duff2013/ulptool). Note: j'ai du modifier quelques lignes dans un fichier python de "ulptool" (src/esp32ulp_build_recipe.py), un probleme de compatibilité entre différentes merdesions de python. (l'interface d'arduino dit où est l'erreur. des "print xxx" à remplacer par des print(xxx), et un open(... "w") par open(... "wb") ). Voir le patch à la fin.

- installez l'outil pour pouvoir "uploader" les mp3 sur le "SPIFFS" (le systeme de fichier) de l'ESP32 (https://randomnerdtutorials.com/install-esp32-filesystem-uploader-arduino-ide/)


===

Le montge:
- connectez l'entrée audio de l'ampli (par exemple un PAM8403) sur le pin 25 de l'ESP32.
- connectez la sortie du module PIR sur le pin 32 de l'ESP
- Connectez le shutdown du PAM8403 (ou autre ampli) sur le pin 13 de l'ESP32. Ce n'est pas nécéssaire pour le faire fonctionner, mais cela descendra la consomation quand le clameur sera silentieux (pour moi, le PAM passe de 10mA à 1uA en faisant cela). Atention, si vous utilisez une board toute faite sans entrée SHUTDOWN, il faudra souder directement sur la pate correspondante (la 12 pour le PAM8402), en l'ayant préalablement déconnecté (en soulevant la pate, ou en coupant sur le PCB). Voir photo TODO
- utilisez un régulateur de tension LDO pour passer des 3.7-4v de la baterie à 3.3V. Préférez un composant "original" (chez moi, un LDO MCP1700 "fake" fait perdre 1mA...)
- connectez le reste comme d'habitude (GND, 3.3V, EN, GPIO0...)



TODO schema/PCB/photos...

TODO STL boitier "solide" 


=====


diff src/esp32ulp_build_recipe.py

--- a/src/esp32ulp_build_recipe.py
+++ b/src/esp32ulp_build_recipe.py
@@ -230,7 +230,7 @@ def build_ulp(PATHS, ulp_sfiles, board_options, has_s_file):
         sys.exit(error_string)
     else:
         file_names_constant = gen_file_names_constant()
-        with open(file_names_constant['sym'],"w") as fsym:
+        with open(file_names_constant['sym'],"wb") as fsym:
             fsym.write(out)
         console_string += cmd[0] + '\r'
 
@@ -302,7 +302,7 @@ def gen_assembly(PATHS):
                         ulpcc_files.append(file)
 
     except Exception as e:
-        print e
+        print(e)
 
     for file in ulpcc_files:
         cmd = gen_lcc_cmd(PATHS, file)

@@ -313,7 +313,7 @@ def gen_assembly(PATHS):
             sys.exit(error_string)
         else:
             if out == "":
-                print cmd[0]
+                print(cmd[0])
             else:
                 sys.exit(str(out))
