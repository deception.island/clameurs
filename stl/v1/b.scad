X=42;
Y=71;
H=24;
B=2;
RSP=20;
RPIR=5;
RSPH=1.5;
XP=17;

module cc(r,n,b)
{
  for(i=[0:360/n:360])
    rotate([0,0,i+b])
      translate([r,0,-5])cylinder(10,RSPH,RSPH,$fn=16);
}

module holes_hp() {
    cylinder(2,RSP,RSP);
    cc(16,16,0);
    cc(13,16,360/32);
    cc(10,8,0);
    cc(7,8,360/16);
    cc(4,4,0);
    cc(0,1,0);
}

// module p(df) {
//     translate([X/2,Y-15,-4])
//   rotate([30,0,0])
//     translate([-XP/2,-XP/2,0]) {
//     if(df==0)
//       translate([-B,-B,-B])
// 	cube([XP+2*B,XP+2*B,H+B]);
//     if(df==1) {
//       cube([XP,XP,H+10]);
//       translate([XP/2,XP/2,-5])
// 	cylinder(10,RPIR,RPIR,$fn=32);
//     }
//   }
  
// }


module e() { //interieur
  translate([-B,-B,-B])
    cube([X+2*B,Y+2*B,H+B]);
}

module b() {
  difference() {
    e();
    cube([X,Y,H+1]);
    translate([X/2,X/2,-1])
      holes_hp();
    translate([X/2,Y-15,0])
    rotate([30,0,0])
      translate([0,-50,-100])
      cube([100,100,100]);
  }
  intersection() {
    translate([X/2,Y-15,0])
      rotate([30,0,0])
      translate([0,-50,-B])
      difference() {
      
      cube([X/2+B,100,B]);
      translate([12,60,-B])
      cylinder(10,RPIR,RPIR,$fn=32);
    }
    e();
  }
  intersection() {
    translate([X/2,Y-15,0])
    rotate([30,0,0])
      translate([0,-50,-100])
      cube([B,100,100]);
    e();

  }

}

module couv() {
  b=0.5;
  translate([-B,-B,-2*B])
    cube([X+2*B,Y+2*B,B]);
  difference() {
    translate([b,b,-B])
      cube([X-2*b,Y-2*b,B]);
    translate([B,B,-B])
      cube([X-2*B,Y-2*B,B]);
  }
}


//b();

couv();
