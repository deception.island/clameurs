#include "soc/rtc_cntl_reg.h"
#include "soc/soc_ulp.h"

	  /* ADC1 channel 6, GPIO34 */
	.set adc_channel, 6

	.data   /* .data section */
	  
	.bss /* Define variables, which go into .bss section (zero-initialized data) */

	.global solar_index
solar_index:
	.long 0

	.global counter
counter:
	.long 0

	.global stage
stage:
	.long 0

	.global acc
acc:
	.long 0

	.global solar_tab
solar_tab:	
	.fill 48,4,0
	

	  /* Code goes into .text section */
	  .text

	.global entry
entry:
	move r3, counter
	ld r0, r3,0
 	add r0, r0, 1
	st r0, r3,0

    	adc r1, 0, adc_channel + 1
	move r3, acc
	ld r0, r3,0
 	add r0, r0, r1
	st r0, r3,0

	move r3, stage
	ld r0, r3,0
 	add r0, r0, 1
	st r0, r3,0

	jumpr exit, 16, lt

	move r3, stage
	move r0, 0
	st r0, r3,0

	move r3, acc
	ld r2, r3,0
	rsh r2, r2, 4

	move r3, solar_index
	ld r0, r3, 0
	//lsh r0, r0, 2

	add r0, r0, solar_tab
	st r2, r0, 0

	ld r0, r3, 0
	add r0, r0, 1
	st r0, r3, 0
	
	jumpr exit, 4, lt

	move r3, solar_index
	move r0, 0
	st r0, r3, 0
	
	jump wake_up

	  .global exit
exit:
	  halt

	  .global wake_up
wake_up:
	  /* Check if the system can be woken up */
	  READ_RTC_FIELD(RTC_CNTL_LOW_POWER_ST_REG, RTC_CNTL_RDY_FOR_WAKEUP)

	  and r0, r0, 1
	  jump exit, eq

	  /* Wake up the SoC, end program */
	  wake
	  WRITE_RTC_FIELD(RTC_CNTL_STATE0_REG, RTC_CNTL_ULP_CP_SLP_TIMER_EN, 0)
	  halt
