/* GPIOs:
 25: analog sound to power amplifier
 32: digital signal from the PIR sensor
 13: to the "shutdown" of power amplifier
 34: to the photocell

SPIFFS file-system:
 /conf.txt : configuration file.
 each line is F <filename> <frequency> <volume> <hour start> <hour end>
 E.g. "F /test.mp3 100 100 6 22"  to play file /test.mp3 at max volume, from 6:00 to 22:00 
 The others files are mp3 files.  
*/

#include "Arduino.h"
//#include <esp_wifi.h>
//#include <esp_bt.h>
//#include <esp_bt_main.h>
#include "soc/efuse_reg.h"
#include "esp_sleep.h"
#include "driver/rtc_io.h"
#include "driver/adc.h"
#include "esp32/ulp.h"
//#include "ulp_main.h"
#include "ulptool.h"
#include "SPIFFS.h"
#include "esp_timer.h"
//#include "esp_log.h"
//#include "sdkconfig.h"

#include <soc/rtc.h>
extern "C" {
  #include <esp_clk.h>
}

#include <string>
#include <vector>
using namespace std;



// ulp programm extern vars
extern uint32_t ulp_entry;
extern uint32_t ulp_counter;
extern uint32_t ulp_solar_index;
extern uint32_t ulp_solar_tab;
extern uint32_t ulp_acc;
extern uint32_t ulp_stage;

#define LOGSERIAL 1

#define uS_TO_S_FACTOR uint64_t(1000000)
RTC_DATA_ATTR short bootCount = 0;
RTC_DATA_ATTR short secs_silent = 10;
RTC_DATA_ATTR short max_by_day = 15;
RTC_DATA_ATTR short old_ulp = 0;

RTC_DATA_ATTR int est_start; // on changera est_time pour que (est_time+time_from_boot)%24 est l'heure estimée


#define SDWN 13 // GPIO for shudwn audio amp


extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_main_bin_start");
extern const uint8_t ulp_main_bin_end[]   asm("_binary_ulp_main_bin_end");

static void init_ulp_program()
{
  esp_err_t err = ulptool_load_binary(0, ulp_main_bin_start,
                                  (ulp_main_bin_end - ulp_main_bin_start) / sizeof(uint32_t));
  ESP_ERROR_CHECK(err);

  if(LOGSERIAL)
    printf("init_ulp_program()\n");
  
  /* Configure ADC channel */
  /* Note: when changing channel here, also change 'adc_channel' constant
     in adc.S */
  adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_ulp_enable();

  ulp_set_wakeup_period(0, 56250000*2/500); //16 times by 30 minutes

  /* Disable pullup on GPIO15, in case it is connected to ground to suppress
     boot messages.
  */
  rtc_gpio_pullup_dis(GPIO_NUM_15);
  rtc_gpio_hold_en(GPIO_NUM_15);
}

static void start_ulp_program(int base=0)
{
  /* Start the program */
  if(LOGSERIAL)
    printf("run ulp starting from %d\n",base);
  ulp_stage=0;
  ulp_acc=0;
  ulp_solar_index=base;
  
  esp_err_t err = ulp_run((&ulp_entry - RTC_SLOW_MEM) / sizeof(uint32_t));
  ESP_ERROR_CHECK(err);
}

void process_light() {
}


struct f_t {
  string fn;
  float proba;
  short volume;
  int when;
};

vector<f_t> files;


string readLine(File &F)
{
  char buffer[100];
  int idx=0;
  while (F.available()) {
    char c=F.read();  
    buffer[idx++]=c;
    if (c=='\n'||idx>=99) {
      buffer[idx]=0;
      return buffer;
    }
  }
  buffer[idx]=0;
  return buffer;
}

void readconf() {
  File f=SPIFFS.open("/conf.txt");
  while(1) {
    string l=readLine(f);
    if(l.empty())
      return;
    if(l[0]=='F') {
      char fn[20];
      int p,v,a,b;
      int n=sscanf(l.c_str()+2,"%s %d %d %d %d",fn,&p,&v,&a,&b);
      if(n==5) {
	f_t r;
	r.fn=fn;
	r.proba=p;
	r.volume=v;
	r.when=0;
	a=0;b=23;
	for(int i=a;i<=b;i++)
	  r.when|=1<<(i%32);
	files.push_back(r);
      }
    }
    if(l[0]=='C') {
      int ss,md;
      int n=sscanf(l.c_str()+2,"%d %d",&ss,&md);
      if(n==2) {
	secs_silent=ss;
	max_by_day=md;
      }
    }
  }
}

uint64_t timeNow;

int choose(int h)
{
  if(LOGSERIAL)
    printf("choose h=%d\n",h);
  float tt=0;
  for(int i=0;i<files.size();i++) {
    if(1&(files[i].when>>i)) {
      tt+=files[i].proba;
    }
  }
  srand(timeNow);
  tt=tt*rand()/RAND_MAX;
  for(int i=0;i<files.size();i++) {
    if(1&(files[i].when>>i)) {
      tt-=files[i].proba;
      if(tt<=0) return i;
    }
  }
  return -1;
}


int getChipRevision()
{
  return (REG_READ(EFUSE_BLK0_RDATA3_REG) >> (EFUSE_RD_CHIP_VER_REV1_S)&&EFUSE_RD_CHIP_VER_REV1_V) ;
}

void printESPRevision() {
  Serial.print("REG_READ(EFUSE_BLK0_RDATA3_REG): ");
  Serial.println(REG_READ(EFUSE_BLK0_RDATA3_REG), BIN);

  Serial.print("EFUSE_RD_CHIP_VER_REV1_S: ");
  Serial.println(EFUSE_RD_CHIP_VER_REV1_S, BIN);

  Serial.print("EFUSE_RD_CHIP_VER_REV1_V: ");
  Serial.println(EFUSE_RD_CHIP_VER_REV1_V, BIN);

  Serial.println();

  Serial.print("Chip Revision (official version): ");
  Serial.println(getChipRevision());

  Serial.print("Chip Revision from shift Operation ");
  Serial.println(REG_READ(EFUSE_BLK0_RDATA3_REG) >> 15, BIN);

}

/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by EXT0 / external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by EXT1 / external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}


#if 1

#include <Arduino.h>
//#include <WiFi.h>
#include "AudioFileSourceSPIFFS.h"
#include "AudioFileSourceID3.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"

// To run, set your ESP8266 build to 160MHz, and include a SPIFFS of 512KB or greater.
// Use the "Tools->ESP8266/ESP32 Sketch Data Upload" menu to write the MP3 to SPIFFS
// Then upload the sketch normally.  

// pno_cs from https://ccrma.stanford.edu/~jos/pasp/Sound_Examples.html

AudioGeneratorMP3 *mp3;
AudioFileSourceSPIFFS *file;
AudioOutputI2S *out;
AudioFileSourceID3 *id3;

// Called when a metadata event occurs (i.e. an ID3 tag, an ICY block, etc.
void MDCallback(void *cbData, const char *type, bool isUnicode, const char *string)
{
  (void)cbData;
  if(LOGSERIAL)
    Serial.printf("ID3 callback for: %s = '", type);

  if (isUnicode) {
    string += 2;
  }
  
  while (*string) {
    char a = *(string++);
    if (isUnicode) {
      string++;
    }
    if(LOGSERIAL)
      Serial.printf("%c", a);
  }
  if(LOGSERIAL)
    Serial.printf("'\n");
  if(LOGSERIAL)
    Serial.flush();
}

bool play=0;

void play_mp3(int i=0) {
  audioLogger = &Serial;

  SPIFFS.begin();

  readconf();
  if(LOGSERIAL)
    printf("start mp3 %d %s\n",i,files[i].fn.c_str());

  file = new AudioFileSourceSPIFFS(files[i].fn.c_str());
  id3 = new AudioFileSourceID3(file);
  id3->RegisterMetadataCB(MDCallback, (void*)"ID3TAG");
  out = new AudioOutputI2S(0, 1); //Uncomment this line, comment the next one to use the internal DAC channel 1 (pin25) on ESP32
  out->SetGain(float(files[i].volume)/100);
  //out = new AudioOutputI2S(0, 1); //Uncomment this line, comment the next one to use the internal DAC channel 1 (pin25) on ESP32
  mp3 = new AudioGeneratorMP3();
  mp3->begin(id3, out);
  play=1;
  gpio_hold_dis(gpio_num_t(SDWN));
  pinMode(SDWN,OUTPUT);
  digitalWrite(SDWN,1);
}

void loop_mp3()
{
  if(!play) return;
  if (mp3->isRunning()) {
    if (!mp3->loop()) mp3->stop();
  } else {
    if(LOGSERIAL)
      Serial.printf("MP3 done\n");
    play=0;
    //delay(1000);
  }
}

#endif


#ifdef MPIR
float baseA=0;
float baseB=0;

int rea1()
{
  return analogRead(32);
}

float rea_m;
float rea_mm;

void rea()
{
#define OS 32
  rea_m=0;
  rea_mm=0;
  for(int i=0;i<OS;i++) {
    float a=rea1();
    rea_m+=a;
    rea_mm+=a*a;
  }
  rea_m/=OS;
  rea_mm/=OS;
  rea_mm-=rea_m*rea_m;
}

void setup_mpir()
{
  rea();
  baseA=baseB=rea_m;
}

int om=-1;

#define F(a,r,x) (a)=(r)*(x)+(a)*(1.-x)

void loop_mpir()
{
  if(play) return;
  
  int u=millis();
  if(om<0 || u<om || u>om+10) {
    
  } else 
    return;
  om=u;

  rea();
  auto r=rea_m;
  F(baseA,r,1./50);
  F(baseB,r,1./10);
  if(LOGSERIAL) {
    char tmp[200];
    sprintf(tmp,"%5d %5d %5d %+4d %5d %+4d %+4d 0123456789",int(r*10),int(rea_mm*1000),int(10*baseA),int(baseA-r),int(10*baseB),int(baseB-r),int(10*(baseA-baseB)));
    Serial.println(tmp);
  }
}
#else
void setup_mpir() {}
void loop_mpir() {}
#endif

// RTC_DATA_ATTR uint64_t sleepTime;
// void setup() {
//   if(esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_TIMER)
//   {
//     uint64_t timeNow, timeDiff;
//     timeNow = rtc_time_slowclk_to_us(rtc_time_get(), esp_clk_slowclk_cal_get());
//     timeDiff = timeNow - sleepTime;
//     printf("Now: %"PRIu64"ms, Duration: %"PRIu64"ms\n", timeNow / 1000, timeDiff / 1000);
//     delay(2000);
//   }
//   sleepTime = rtc_time_slowclk_to_us(rtc_time_get(), esp_clk_slowclk_cal_get());
//   printf("Sleeping...\n");
//   esp_sleep_enable_timer_wakeup(5 * 1000000);
//   esp_deep_sleep_start();
// }

void deep_sleep(int secs)
{
  esp_sleep_enable_timer_wakeup(secs * uS_TO_S_FACTOR);
  ESP_ERROR_CHECK( esp_sleep_enable_ulp_wakeup() );

  if(LOGSERIAL) {
    Serial.print("Going to sleep now: ");
    Serial.print(secs);
    Serial.println(" second(s)");
    Serial.flush(); 
    delay(10);
    Serial.flush(); 
  }
  
  pinMode(SDWN,OUTPUT);
  digitalWrite(SDWN,0);
  gpio_hold_en(gpio_num_t(SDWN));
  gpio_deep_sleep_hold_en();

  esp_deep_sleep_start();
}

bool can_play_time() {
  return 1;
}

// void hibernate() {
//     esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH,   ESP_PD_OPTION_OFF);
//     esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_SLOW_MEM, ESP_PD_OPTION_OFF);
//     esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_FAST_MEM, ESP_PD_OPTION_OFF);
//     esp_sleep_pd_config(ESP_PD_DOMAIN_XTAL,         ESP_PD_OPTION_OFF);
    
//     deepSleep();
// }



bool wakeplay=0;

void print_solar() {
  printf("solar_index: %x acc:%x stage: %x counter:%d old_cnt:%d\n",0xffff&ulp_solar_index,0xffff&ulp_acc,0xffff&ulp_stage,0xffff&ulp_counter,old_ulp);
  for(int i=0;i<48;i++)
    printf("%d ",int(0xffff&(&ulp_solar_tab)[i]));
  printf("\n");
}

void setup() {
  //WiFi.mode(WIFI_OFF); 
  if(1 || LOGSERIAL) {
    Serial.begin(115200);
    delay(10);
  }
  

  if(LOGSERIAL)
    printESPRevision();
  
  ++bootCount;
  if(LOGSERIAL)
    Serial.printf("boot count: %d\n",bootCount);
  if(LOGSERIAL)
    print_wakeup_reason();
  //char tmp[200];
  //sprintf(tmp,"time since boot: %lld us",esp_timer_get_time());
  //Serial.println(tmp);

  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();

  //wakeplay=1;
  bool wakeulp=0;
  bool waketimer=0;
  switch(wakeup_reason) {
  case ESP_SLEEP_WAKEUP_EXT1 : 
  case ESP_SLEEP_WAKEUP_EXT0 :
    wakeplay=1;
    break;
  case ESP_SLEEP_WAKEUP_TIMER :
    waketimer=1;
    break;
  case ESP_SLEEP_WAKEUP_ULP : 
    wakeulp=1;
    break;
  case ESP_SLEEP_WAKEUP_TOUCHPAD : 
  default : //Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
    break;
  }

  if(wakeplay==0 && wakeulp==0 && waketimer==0) {
    init_ulp_program();

    start_ulp_program();
    //ESP_ERROR_CHECK( esp_sleep_enable_ulp_wakeup() );
  }

  timeNow = rtc_time_slowclk_to_us(rtc_time_get(), esp_clk_slowclk_cal_get());
  if(LOGSERIAL)
    printf("Now: %" PRIu64 "ms,\n", timeNow / 1000);

  if(wakeulp) {
    print_solar();
    //proc solar
    //restart ulp
    start_ulp_program();
  }

  if(waketimer) {
    print_solar();
    if(old_ulp==ulp_counter) {
      //suppose that the ulp programm is stopped
      start_ulp_program();
    }
    old_ulp=ulp_counter;
  }
  
  //esp_bluedroid_disable();
  //esp_bt_controller_disable();
  //esp_wifi_stop();
  //setup_mpir();
  if(!wakeplay) {
    //esp_sleep_enable_ext0_wakeup(GPIO_NUM_32,1); //PIR sensor
    esp_sleep_enable_ext1_wakeup(1LL<<32,ESP_EXT1_WAKEUP_ANY_HIGH); //PIR sensor

    deep_sleep(2);
  }
  
  //esp_light_sleep_start();
}

void loop() {
  // put your main code here, to run repeatedly:
  //assert(wakeplay);
  if(!play) {
    int r=choose((0xffff&ulp_solar_index)/2);
    if(r>=0)
      play_mp3(r);
  }
  loop_mp3();
  //loop_mpir();

  if(0 &&LOGSERIAL) {
    char tmp[200];
    sprintf(tmp,"in loop() / time since boot: %lld us",esp_timer_get_time());
    Serial.println(tmp);
  }

  if(!play) {
    //assert(wakeplay);
    esp_sleep_enable_timer_wakeup(secs_silent * uS_TO_S_FACTOR);
    //esp_sleep_enable_ext0_wakeup(GPIO_NUM_32,1); //PIR sensor
    if(LOGSERIAL) {
      Serial.println("Going to sleep now");
      Serial.flush(); 
      delay(100);
      Serial.flush(); 
    }
    esp_deep_sleep_start();
  }
  
  ///delay(500);
}




